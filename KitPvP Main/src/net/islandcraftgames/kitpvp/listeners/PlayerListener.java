package net.islandcraftgames.kitpvp.listeners;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import com.earth2me.essentials.IEssentials;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.islandcraftgames.kitpvp.events.PlayerJoinArenaEvent;
import net.islandcraftgames.kitpvp.player.PlayerManager;

public class PlayerListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		PlayerManager.remove(e.getPlayer());
		try {
			IEssentials ess = (IEssentials) Bukkit.getPluginManager().getPlugin("Essentials");
			e.getPlayer().teleport(ess.getWarps().getWarp("spawn"));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	@EventHandler
	public void onSpawnExit(PlayerMoveEvent e) {
		if (e.getPlayer().getGameMode() != GameMode.SURVIVAL && e.getPlayer().getGameMode() != GameMode.ADVENTURE)
			return;

		Location from = e.getFrom();
		Location to = e.getTo();
		if (from.getBlockX() == to.getBlockX() && from.getBlockY() == to.getBlockY()
				&& from.getBlockZ() == to.getBlockZ())
			return;

		RegionManager rm = WorldGuardPlugin.inst().getRegionManager(to.getWorld());
		ProtectedRegion pr = rm.getRegionExact("spawn");
		if (!pr.contains(from.getBlockX(), from.getBlockY(), from.getBlockZ()))
			return;
		if (pr.contains(to.getBlockX(), to.getBlockY(), to.getBlockZ()))
			return;
		PlayerJoinArenaEvent event = new PlayerJoinArenaEvent(e.getPlayer(), PlayerManager.getKit(e.getPlayer()));
		Bukkit.getPluginManager().callEvent(event);
		if (event.isCancelled())
			e.setTo(from);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoinArena(PlayerJoinArenaEvent e) {
		if (e.isCancelled())
			return;
		if (e.getKit() == null)
			return;
		e.getKit().giveItems(e.getPlayer());
	}

	@EventHandler
	public void onSoupEat(PlayerInteractEvent e) {
		if (e.getPlayer().getGameMode() != GameMode.SURVIVAL && e.getPlayer().getGameMode() != GameMode.ADVENTURE)
			return;

		if (!e.getAction().toString().contains("RIGHT"))
			return;

		ItemStack is = e.getItem();
		if (is == null)
			return;
		if (is.getType() != Material.MUSHROOM_SOUP)
			return;

		e.setCancelled(true);
		
		if (e.getPlayer().getHealth() == e.getPlayer().getMaxHealth())
			return;

		int toGive = 6;
		if (e.getPlayer().getHealth() + toGive <= e.getPlayer().getMaxHealth())
			e.getPlayer().setHealth(e.getPlayer().getHealth() + toGive);
		else
			e.getPlayer().setHealth(e.getPlayer().getMaxHealth());

		e.getPlayer().setItemInHand(null);
	}

	@EventHandler
	public void noHungry(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}

}
