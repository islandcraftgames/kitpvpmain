package net.islandcraftgames.kitpvp.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.islandcraftgames.kitpvp.kits.Kit;

public class PlayerJoinArenaEvent extends Event implements Cancellable{
	
	private boolean cancelled = false;
	private static final HandlerList handlers = new HandlerList();
	private Player p;
	private Kit kit;
	
	public PlayerJoinArenaEvent(Player p, Kit kit){
		this.p = p;
		this.kit = kit;
	}
	
	public Player getPlayer(){
		return this.p;
	}
	
	public Kit getKit(){
		return this.kit;
	}
	
	public void setKit(Kit kit){
		this.kit = kit;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

}
