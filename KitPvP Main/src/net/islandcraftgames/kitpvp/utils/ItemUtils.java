package net.islandcraftgames.kitpvp.utils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

public class ItemUtils {

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName) {
		ItemStack a = new ItemStack(material, amount);
		if (displayName.length() > 0) {
			ItemMeta b = a.getItemMeta();
			b.setDisplayName(displayName);
			a.setItemMeta(b);
		}
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName,
			@Nonnull String[] lore) {
		ItemStack a = new ItemStack(material, amount);
		ItemMeta b = a.getItemMeta();
		if (displayName.length() > 0)
			b.setDisplayName(displayName);
		List<String> c = new ArrayList<String>();
		for (String d : lore)
			c.add(d);
		b.setLore(c);
		a.setItemMeta(b);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull int durability,
			@Nonnull String displayName) {
		ItemStack a = createItem(material, amount, displayName);
		a.setDurability((short) durability);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull int durability,
			@Nonnull String displayName, @Nonnull String[] lore) {
		ItemStack a = createItem(material, amount, displayName, lore);
		a.setDurability((short) durability);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName,
			@Nonnull Object[][] enchantments) {
		ItemStack a = createItem(material, amount, displayName);
		for (Object[] enc : enchantments) {
			a.addEnchantment((Enchantment) enc[0], (int) enc[1]);
		}
		return a;
	}

	public static ItemStack changeAmount(@Nonnull ItemStack itemStack, @Nonnull int amount) {
		ItemStack a = itemStack.clone();
		a.setAmount(amount);
		return a;
	}

	public static ItemStack setKitID(@Nonnull ItemStack itemStack, @Nonnull int id) {
		String s = ItemMetaDataUtils.encodeString(id + "");
		List<String> lore = Lists.newArrayList();
		lore.add(s);
		if (itemStack.hasItemMeta())
			if (itemStack.getItemMeta().hasLore())
				lore.addAll(itemStack.getItemMeta().getLore());
		ItemMeta im = itemStack.getItemMeta();
		im.setLore(lore);
		itemStack.setItemMeta(im);
		return itemStack;
	}

	public static int getKitID(@Nonnull ItemStack is) {
		if (!is.hasItemMeta())
			return -1;
		ItemMeta im = is.getItemMeta();
		if (!im.hasLore())
			return -1;
		if (!ItemMetaDataUtils.hasHiddenString(im.getLore().get(0)))
			return -1;
		return Integer.parseInt(ItemMetaDataUtils.extractHiddenString(im.getLore().get(0)));
	}

	public static void fillWithSoup(Inventory inv) {
		while (inv.firstEmpty() != -1)
			inv.addItem(new ItemStack(Material.MUSHROOM_SOUP));
	}

}
