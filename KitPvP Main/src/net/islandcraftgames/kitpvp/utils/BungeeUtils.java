package net.islandcraftgames.kitpvp.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.bukkit.entity.Player;

import net.islandcraftgames.kitpvp.Main;

public class BungeeUtils {
	
	public static void sendToServer(Player p, String target){
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);
		try{
			out.writeUTF("Connect");
			out.writeUTF(target);
		}catch(Exception e){
			e.printStackTrace();
		}
		p.sendPluginMessage(Main.get(), "BungeeCord", b.toByteArray());
	}

}
