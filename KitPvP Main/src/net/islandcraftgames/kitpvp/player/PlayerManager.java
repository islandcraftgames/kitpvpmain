package net.islandcraftgames.kitpvp.player;

import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import net.islandcraftgames.kitpvp.kits.Kit;

public class PlayerManager {

	private static Map<Player, Kit> map = Maps.newHashMap();
	
	public static void setKit(Player p, Kit k){
		map.put(p, k);
	}
	
	public static Kit getKit(Player p){
		return map.get(p);
	}
	
	public static void remove(Player p){
		map.remove(p);
	}
	
}
