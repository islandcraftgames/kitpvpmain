package net.islandcraftgames.kitpvp.kits;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.Maps;

import net.islandcraftgames.kitpvp.Main;
import net.islandcraftgames.kitpvp.utils.ItemUtils;

public class KitInventoryManager {

	private static Map<Inventory, Integer> invs = Maps.newHashMap();

	public static void openInventory(Player p) {
		List<Kit> kits = new ArrayList<>(KitsMain.getAll());
		if (kits.size() > 54) {
			openInventory(p, 1);
			return;
		}
		Inventory inv = Bukkit.createInventory(null, getInventorySize(kits.size()), "Select a kit");
		for (Kit kit : kits)
			inv.addItem(kit.getShowcaseItem(p));
		invs.put(inv, 1);
		p.openInventory(inv);
	}

	public static void openInventory(Player p, int page) {
		List<Kit> kits = new ArrayList<>(KitsMain.getAll());
		Inventory inv = Bukkit.createInventory(null, 54, "Select a kit - Page " + page);
		for (Kit kit : cutArray(kits, page))
			inv.addItem(kit.getShowcaseItem(p));
		inv.setItem(53, ItemUtils.createItem(Material.PAPER, 1, "Next Page"));
		inv.setItem(44, ItemUtils.createItem(Material.PAPER, 1, "Previous Page"));
		if (page == 1)
			inv.setItem(44, null);
		if (isLast(kits.size(), page))
			inv.setItem(53, null);
		invs.put(inv, page);
		p.openInventory(inv);
	}

	private static int getInventorySize(int itemsNum) {
		int ii = 1;
		while (itemsNum > 9) {
			ii++;
			itemsNum -= 9;
		}
		return ii * 9;
	}

	private static List<Kit> cutArray(List<Kit> array, int page) {
		if (page > (page - 1) && array.size() <= (45 * (page - 1)))
			return Collections.emptyList();
		List<Kit> firstPart = array.subList((45 * (page - 1)), array.size());
		if (firstPart.size() <= 45)
			return firstPart;
		return firstPart.subList(0, 45);
	}

	private static boolean isLast(int arraySize, int page) {
		return arraySize <= (page * 45);
	}

	public static boolean isOpen(Inventory inv) {
		return invs.containsKey(inv);
	}

	public static void closeInventory(Inventory inv) {
		invs.remove(inv);
	}

	public static void nextPage(Inventory inv, final Player p) {
		final int nextPage = invs.get(inv) + 1;
		p.closeInventory();
		Bukkit.getScheduler().runTaskLater(Main.get(), new Runnable() {

			@Override
			public void run() {
				openInventory(p, nextPage);
			}
		}, 1L);
	}
	
	public static void previousPage(Inventory inv, final Player p) {
		final int nextPage = invs.get(inv) - 1;
		p.closeInventory();
		Bukkit.getScheduler().runTaskLater(Main.get(), new Runnable() {

			@Override
			public void run() {
				openInventory(p, nextPage);
			}
		}, 1L);
	}

}
