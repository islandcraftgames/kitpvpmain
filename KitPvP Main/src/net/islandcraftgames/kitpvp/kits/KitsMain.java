package net.islandcraftgames.kitpvp.kits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;

import com.google.common.collect.Lists;

import net.islandcraftgames.kitpvp.Main;
import net.islandcraftgames.kitpvp.kits.kits.Grappler;
import net.islandcraftgames.kitpvp.kits.kits.Kangaroo;
import net.islandcraftgames.kitpvp.kits.kits.Ninja;
import net.islandcraftgames.kitpvp.kits.kits.Stomper;

public class KitsMain {

	private static ArrayList<Kit> kits = Lists.newArrayList();

	private static boolean settedUp = false;

	private static void setup() {
		if (settedUp)
			return;
		kits.clear();

		a(new Stomper());
		a(new Grappler());
		a(new Kangaroo());
		a(new Ninja());

		settedUp = true;
	}

	public static List<Kit> getAll() {
		setup();
		return new ArrayList<Kit>(kits);
	}

	public static Kit getKit(int id) {
		return getKit(id, true);
	}

	private static Kit getKit(int id, boolean setup) {
		if (setup)
			setup();
		for (Kit kit : kits)
			if (kit.getID() == id)
				return kit;
		return null;
	}

	private static void a(Kit kit) {
		try {
			if (getKit(kit.getID(), false) != null)
				return;
			kits.add(kit);
			if (kit.isListener())
				Bukkit.getPluginManager().registerEvents(kit, Main.get());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
