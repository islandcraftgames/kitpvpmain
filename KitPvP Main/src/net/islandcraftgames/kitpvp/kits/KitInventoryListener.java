package net.islandcraftgames.kitpvp.kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import net.islandcraftgames.kitpvp.player.PlayerManager;
import net.islandcraftgames.kitpvp.utils.ItemUtils;

public class KitInventoryListener implements Listener {

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (!KitInventoryManager.isOpen(e.getInventory()))
			return;

		e.setCancelled(true);

		if (e.getCurrentItem() == null)
			return;
		if (e.getClickedInventory() == null)
			return;
		if (!KitInventoryManager.isOpen(e.getClickedInventory()))
			return;

		if (e.getCurrentItem().getType() == Material.AIR)
			return;

		if (e.getRawSlot() == 53 && e.getCurrentItem().getType() == Material.PAPER) {
			KitInventoryManager.nextPage(e.getClickedInventory(), (Player) e.getWhoClicked());
			return;
		}

		if (e.getRawSlot() == 44 && e.getCurrentItem().getType() == Material.PAPER) {
			KitInventoryManager.previousPage(e.getClickedInventory(), (Player) e.getWhoClicked());
			return;
		}

		int id = ItemUtils.getKitID(e.getCurrentItem());

		if (id == -1)
			return;

		PlayerManager.setKit((Player) e.getWhoClicked(), KitsMain.getKit(id));
		e.getWhoClicked().sendMessage(ChatColor.GREEN + "Selected kit: " + ChatColor.GOLD + ChatColor.stripColor(
				KitsMain.getKit(id).getShowcaseItem((Player) e.getWhoClicked()).getItemMeta().getDisplayName()));
		e.getWhoClicked().closeInventory();
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (KitInventoryManager.isOpen(e.getInventory()))
			KitInventoryManager.closeInventory(e.getInventory());
	}

}
