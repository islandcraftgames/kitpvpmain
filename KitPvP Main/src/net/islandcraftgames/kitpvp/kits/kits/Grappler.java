package net.islandcraftgames.kitpvp.kits.kits;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import net.islandcraftgames.kitpvp.Main;
import net.islandcraftgames.kitpvp.kits.Kit;
import net.islandcraftgames.kitpvp.player.PlayerManager;
import net.islandcraftgames.kitpvp.utils.ItemUtils;
import net.minecraft.server.v1_8_R3.EntityFishingHook;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityEquipment;

public class Grappler extends Kit {

	@Override
	public void giveItems(Player p) {

	}

	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemUtils.setKitID(ItemUtils.createItem(Material.LEASH, 1, ChatColor.GREEN + "Grappler"), getID());
	}

	@Override
	public int getID() {
		return 1;
	}

	@Override
	public boolean isListener() {
		return true;
	}

	@Override
	public boolean canUse(Player p) {
		return true;
	}

	Map<Player, Hook> hooks = Maps.newHashMap();

	@EventHandler
	public void onSlot(PlayerItemHeldEvent e) {
		if (this.hooks.containsKey(e.getPlayer()))
			hooks.remove(e.getPlayer()).remove();
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if ((this.hooks.containsKey(e.getPlayer()))
				&& ((!e.getPlayer().getItemInHand().getType().equals(Material.LEASH))
						&& PlayerManager.getKit(e.getPlayer()).getID() == getID())) {
		}
	}

	@EventHandler
	public void onLeash(PlayerLeashEntityEvent e) {
		Player p = e.getPlayer();
		if ((e.getPlayer().getItemInHand().getType().equals(Material.LEASH))
				&& PlayerManager.getKit(p).getID() == getID()) {
		}
	}

	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if ((e.getPlayer().getItemInHand().getType().equals(Material.LEASH))
				&& PlayerManager.getKit(p).getID() == getID()) {
			e.setCancelled(true);
			if ((e.getAction() == Action.LEFT_CLICK_AIR) || (e.getAction() == Action.LEFT_CLICK_BLOCK)) {
				if (this.hooks.containsKey(p))
					this.hooks.remove(p).remove();
				spawnHook(p);
			} else if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
				if (!this.hooks.containsKey(p))
					return;
				p.sendMessage(this.hooks.get(p).getLandedLocation().toString());
			}
		}
	}

	private void spawnHook(final Player p) {
		final ItemStack inHand = p.getItemInHand();
		p.setItemInHand(new ItemStack(Material.FISHING_ROD));
		Hook hook = new Hook(p.getWorld(), p);
		((CraftWorld) p.getWorld()).getHandle().addEntity(hook);
		//hook.inHand = inHand;
	}

	@EventHandler
	public void onControllerLand(ProjectileHitEvent e) {
		if (!(e.getEntity() instanceof Arrow))
			return;
		Arrow arrow = (Arrow) e.getEntity();
		if (!(arrow.getShooter() instanceof Player))
			return;
		Player p = (Player) arrow.getShooter();
		if (!this.hooks.containsKey(p))
			return;
		Hook hook = this.hooks.get(p);
		if (hook.getController() != arrow)
			return;
		//Bukkit.getScheduler().cancelTask(hook.getTaskId());
		arrow.setPassenger(null);
		arrow.remove();
	}

	private class Hook extends EntityFishingHook {
		private int taskId = 0;
		private Arrow controller;
		private World w;
		private Player owner;
		//public ItemStack inHand;

		public Hook(World w, Player p) {
			super(((CraftWorld) w).getHandle(), ((CraftPlayer) p).getHandle());
			this.w = w;
			this.owner = p;
			spawnController();
		}

		public void remove() {
			Bukkit.getScheduler().cancelTask(getTaskId());
			super.die();
		}

		private void spawnController() {
			this.controller = this.owner.launchProjectile(Arrow.class);
			controller.setPassenger(getBukkitEntity());
			this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.get(), new Runnable() {
				@Override
				public void run() {
					// Location landed = controller.getLocation();
					// teleportTo(landed, false);
					// Vector v = controller.getVelocity();

					// sPacketPlayOutEntityDestroy pt = new
					// PacketPlayOutEntityDestroy(getNmsController().getId());
					// for (Player p : w.getPlayers())
					// ((CraftPlayer)
					// p).getHandle().playerConnection.sendPacket(pt);
					/*if (inHand == null)
						return;
					PacketPlayOutEntityEquipment pt = new PacketPlayOutEntityEquipment(getNmsOwner().getId(), 0,
							CraftItemStack.asNMSCopy(inHand));
					for (Player p : w.getPlayers())
						((CraftPlayer) p).getHandle().playerConnection.sendPacket(pt);*/
				}
			}, 0L, 0L);
		}

		/*
		 * public EntityArrow getNmsController() { return ((CraftArrow)
		 * controller).getHandle(); }
		 */

		public EntityPlayer getNmsOwner() {
			return ((CraftPlayer) owner).getHandle();
		}

		public int getTaskId() {
			return this.taskId;
		}

		public Arrow getController() {
			return controller;
		}

		public Location getLandedLocation() {
			return new Location(this.w, this.locX, this.locY, this.locZ);
		}

	}

}
