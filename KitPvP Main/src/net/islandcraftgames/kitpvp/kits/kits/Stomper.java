package net.islandcraftgames.kitpvp.kits.kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import net.islandcraftgames.kitpvp.kits.Kit;
import net.islandcraftgames.kitpvp.player.PlayerManager;
import net.islandcraftgames.kitpvp.utils.ItemUtils;

public class Stomper extends Kit {

	@Override
	public void giveItems(Player p) {
		PlayerInventory inv = p.getInventory();
		inv.clear();
		inv.addItem(new ItemStack(Material.STONE_SWORD));
		inv.setArmorContents(
				new ItemStack[] { new ItemStack(Material.CHAINMAIL_BOOTS), new ItemStack(Material.CHAINMAIL_LEGGINGS),
						new ItemStack(Material.LEATHER_CHESTPLATE), new ItemStack(Material.CHAINMAIL_HELMET) });
		ItemUtils.fillWithSoup(inv);
	}

	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemUtils.setKitID(ItemUtils.createItem(Material.ANVIL, 1, ChatColor.GREEN + "Stomper"), getID());
	}

	@Override
	public int getID() {
		return 0;
	}

	@Override
	public boolean isListener() {
		return true;
	}

	@Override
	public boolean canUse(Player p) {
		return true;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent e) {
		if (e.isCancelled())
			return;
		if ((e.getEntity() instanceof Player)) {
			Player p = (Player) e.getEntity();
			if ((e.getCause() == EntityDamageEvent.DamageCause.FALL) && PlayerManager.getKit(p).getID() == getID()
					&& (e.getDamage() >= 2)) {
				p.getWorld().playSound(p.getLocation(), Sound.ANVIL_LAND, 1.0F, 0.5F);
				for (Entity en : p.getNearbyEntities(4, 2, 4)) {
					if (!(en instanceof Damageable))
						continue;
					((Damageable) en).damage(e.getDamage(), p);
					e.setCancelled(true);
				}
				e.setDamage(e.getDamage() * 0.05);
			}
		}
	}

}
