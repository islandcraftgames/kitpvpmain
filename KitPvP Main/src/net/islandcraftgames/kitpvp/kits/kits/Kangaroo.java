package net.islandcraftgames.kitpvp.kits.kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.islandcraftgames.kitpvp.kits.Kit;
import net.islandcraftgames.kitpvp.utils.ItemUtils;

public class Kangaroo extends Kit{

	@Override
	public void giveItems(Player p) {
		
	}

	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemUtils.setKitID(ItemUtils.createItem(Material.FIREWORK, 1, ChatColor.GREEN + "Kangaroo"), getID());
	}

	@Override
	public int getID() {
		return 2;
	}

	@Override
	public boolean isListener() {
		return false;
	}

	@Override
	public boolean canUse(Player p) {
		return true;
	}
	

}
