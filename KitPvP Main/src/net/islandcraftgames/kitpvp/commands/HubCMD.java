package net.islandcraftgames.kitpvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.earth2me.essentials.IEssentials;

import net.islandcraftgames.kitpvp.utils.BungeeUtils;

public class HubCMD implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only Players!");
			return false;
		}

		Player p = (Player) s;
		try {
			IEssentials ess = (IEssentials) Bukkit.getPluginManager().getPlugin("Essentials");
			ess.getUser(p).getTeleport().teleport(new Location(p.getWorld(), 0, 1, 0), null, TeleportCause.PLUGIN);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		if (e.getCause() != TeleportCause.PLUGIN)
			return;

		if (!(e.getTo().getBlockX() == 0 && e.getTo().getBlockY() == 1 && e.getTo().getBlockZ() == 0))
			return;

		BungeeUtils.sendToServer(e.getPlayer(), "hub");
	}

}
