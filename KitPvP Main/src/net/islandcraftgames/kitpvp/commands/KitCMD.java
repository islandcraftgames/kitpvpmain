package net.islandcraftgames.kitpvp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.islandcraftgames.kitpvp.kits.KitInventoryManager;

public class KitCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only Players!");
			return false;
		}

		KitInventoryManager.openInventory((Player) s);

		return true;
	}

}
