package net.islandcraftgames.kitpvp;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.islandcraftgames.kitpvp.commands.HubCMD;
import net.islandcraftgames.kitpvp.commands.KitCMD;
import net.islandcraftgames.kitpvp.kits.KitInventoryListener;
import net.islandcraftgames.kitpvp.listeners.PlayerListener;

public class Main extends JavaPlugin{
	
	private static Main instance;
	
	@Override
	public void onEnable() {
		instance = this;
		Bukkit.getPluginManager().registerEvents(new KitInventoryListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
		Bukkit.getPluginManager().registerEvents(new HubCMD(), this);
		getCommand("kit").setExecutor(new KitCMD());
		getCommand("hub").setExecutor(new HubCMD());
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	}
	
	public static Main get(){
		return instance;
	}

}
